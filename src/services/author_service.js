import api from "../api/api"

export const postAuthor = async(author)=>{
   let response = await api.post('author',author)
   return response.data.message
}

export const uploadImage = async(file)=>{
   let formData=new FormData()
   formData.append('image', file)

   let response = await api.post('images',formData)
   return response.data.url
}

export const deleteAuthor = async (id) => {
   let response = await api.delete('author/' + id)
   return response.data.message
}

export const fetchAuthor = async () => {
   try{
      const apiGetAuthor = await api.get("author");
      return apiGetAuthor.data.data;
   }catch (error){
      console.log("fetchAuthor Error:",error);
   }
};
export const editAuthorById = async (id, newAuthor) => {
   let response = await api.put('author/' + id, newAuthor)
   return response.data.message
}
export const fetchAuthorById = async (id) => {
   let response = await api.get('author/' + id)
   return response.data.data
}
 