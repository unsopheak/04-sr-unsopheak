import React, { useState, useEffect } from "react";
import { Col, Container, Row, Form, Button, Table } from "react-bootstrap";
import {
  postAuthor,
  uploadImage,
  fetchAuthor,
  deleteAuthor,
  editAuthorById,
  fetchAuthorById,
} from "../services/author_service";

export default function Author() {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [editID, seteditID] = useState(0);
  const [imageURL, setImageURL] = useState(
    "https://peacemakersnetwork.org/wp-content/uploads/2019/09/placeholder.jpg"
  );
  const [imageFile, setImageFile] = useState(null);
  const [author, setAuthor] = useState([]);
  const [submit, setsubmit] = useState(0);
  const [saveStatus, setsaveStatus] = useState(0);

  useEffect(async () => {
    onFetch();
    console.log(submit);
  }, [submit]);

  const onDelete = async (id) => {
    await deleteAuthor(id).then((message) => {
      let newAuthor = author.filter((author) => author._id !== id);
      setAuthor(newAuthor);
      alert(message);
    });
  };

  const onAdd = async (e) => {
    e.preventDefault();

    let author = {
      name,
      email,
    };

    if (imageFile) {
      let url = await uploadImage(imageFile);
      author.image = url;
    }
    Promise.all([
      postAuthor(author).then((message) => alert(message)),
      setsubmit(submit + 1),
    ]);
    onFetch();
  };

  const onFetch = async () => {
    const result = await fetchAuthor();
    setAuthor(result);
    console.log(result);
  };

  const onSave = async (e) => {
    e.preventDefault();
    let author = {
      name,
      email,
    };
    if (imageFile) {
      let url = await uploadImage(imageFile);
      author.image = url;
    }
    console.log(editID);
    editAuthorById(editID, author)
      .then((message) => alert(message))
      .then(setsubmit(submit + 1));
    setsaveStatus(0);
    onFetch();
    setsubmit(0);
  };

  const onEdit = async (id) => {
    const authorResult = await fetchAuthorById(id);
    setName(authorResult.name);
    setEmail(authorResult.email);
    setImageURL(authorResult.image);
    setsaveStatus(1);
    seteditID(id);
  };

  return (
    <Container>
      <h1 className="my-2">Author</h1>
      <Row>
        <Col md={9}>
          <Form>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Author Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Author Name"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
              <Form.Text className="text-muted"></Form.Text>
            </Form.Group>

            <Form.Group controlId="formBasicPassword">
              <Form.Label>Email</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter Email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </Form.Group>
            <br></br>
            <Button
              variant="primary"
              type="submit"
              onClick={onAdd}
              className={saveStatus !== 1 ? "d-block" : "d-none"}
            >
              Add
            </Button>
            <Button
              variant="primary"
              type="submit"
              onClick={onSave}
              className={saveStatus === 1 ? "d-block" : "d-none"}
            >
              Save
            </Button>
          </Form>
        </Col>
        <Col md={3}>
          <img className="w-100" src={imageURL} alt="" />
          <Form>
            <Form.Group>
              <Form.File
                id="img"
                label="Choose Image"
                onChange={(e) => {
                  let url = URL.createObjectURL(e.target.files[0]);
                  setImageFile(e.target.files[0]);
                  setImageURL(url);
                }}
              />
            </Form.Group>
          </Form>
        </Col>
      </Row>
      <br></br>
      <div>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Email</th>
              <th>Image</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {author.map((item, index) => {
              return (
                <tr key={index}>
                  <td>{item._id}</td>
                  <td>{item.name}</td>
                  <td>{item.email}</td>
                  <td>
                    <img width="200px" src={item.image} />
                  </td>
                  <td>
                    <Button
                      onClick={() => {
                        onEdit(item._id);
                      }}
                      variant="warning"
                    >
                      Edit
                    </Button>{" "}
                    <Button
                      onClick={() => {
                        onDelete(item._id);
                      }}
                      variant="danger"
                    >
                      Delete
                    </Button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </div>
    </Container>
  );
}
